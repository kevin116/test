# Markdown test: Code block + List

* List + code block, without empty line
```
line 1
line 2
line 3
```

* List with code block + empty line
```
line 1, then empty line

line 3
```